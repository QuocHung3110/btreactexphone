import React, { Component } from "react";

export default class Item extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.data;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary  h-100" style={{ width: "10rem" }}>
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.XemSp(this.props.data);
              }}
              className="btn btn-success"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
