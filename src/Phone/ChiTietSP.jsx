import React, { Component } from "react";

export default class ChiTietSP extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.ChiTietSP;

    return (
      <div>
        <div className="row">
          <div className="col-md-4">
            <br />
            <h3 className="text-center">{tenSP}</h3>
            <img
              className="card-img-top"
              style={{ width: "18rem" }}
              src={hinhAnh}
              alt={tenSP}
            />
          </div>
          <div className="col-8">
            <table className="table">
              <thead>
                <tr>
                  <td colSpan="2">
                    <h3>Thông số kỹ thuật</h3>
                  </td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <h3>{maSP}</h3>
                  </td>
                </tr>
                <tr>
                  <td>Màn hình</td>
                  <td>{manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera Sau</td>
                  <td>{cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{rom}</td>
                </tr>
                <tr>
                  <td>Giá bán</td>
                  <td>{giaBan}</td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
